from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select
import time
import json

driver=webdriver.Firefox()
#driver.implicitly_wait(10)
driver.get("http://www.optasports.com/football-team-and-player-widget-showcase.aspx")

#print select.options
teams=[]
count=1

#select = Select(driver.find_element_by_id("team_p0"))
select = driver.find_element_by_id('team_p0')
for team in select.find_elements_by_tag_name('option'):#works only for single team
	if "Select team" in team.text:
		continue
	#print count," Team Name: ",team.text
	teams.append(team.text)
	count=count+1


dict_football={}
c=0
for team_name in teams:
	if c>3:
		break
	select = Select(driver.find_element_by_id("team_p0"))
	select.select_by_visible_text(team_name)
	time.sleep(1)
	#print "Team Name: ",team_name
	
	players_names=[]
	players=driver.find_element_by_id('player_p0')
	for player in players.find_elements_by_tag_name('option'):
		if "Select player" in player.text:
			continue
		#print "Player=",player.text
		players_names.append(player.text)
		#player.click()
	
	player_details=[]
	#Get each player details
	for player in players_names:
		select = Select(driver.find_element_by_id("player_p0"))
		select.select_by_visible_text(player)
		time.sleep(1)

		about_player=driver.find_element_by_id("playercompare-table-2").text
		details=about_player.split("\n")
		#print "each details"

		"""
		Aaron Ramsey
		Comparison
		MF Position -
		29 Games played 0
		2,009 Minutes Played 0
		23 Starts 0
		6 Substitution On 0
		9 Substitution Off 0

		"""
		player_name=details[0]
		player_position=details[2].split()[0]
		player_games_played=details[3].split()[0]
		player_minutes_played=details[4].split()[0]
		player_stars=details[5].split()[0]
		player_substitution_on=""
		player_substitution_off=""
		if 6<len(details):
			player_substitution_on=details[6].split()[0]
		if 7<len(details):
			player_substitution_off=details[7].split()[0]

		player_dict={
			"Name":player_name,
			"Position":player_position,
			"Games Played":player_games_played,
			"Minutes Played":player_minutes_played,
			"Stars":player_stars,
			"Substitution On":player_substitution_on,
			"Substitution Off":player_substitution_off
		}
		player_details.append(player_dict)
		
		dict_football[team_name]=player_details
		print player_dict
		print ","
		#now extract player details
		#basic details are in td tag playercompare-table-2
		#comparision details are in div tag playercompare-container-2

	#print "\n\n\n"
	c=c+1
print dict_football
with open("football.json","w") as outfile:
	json.dump(dict_football,outfile,indent=4)
	outfile.close